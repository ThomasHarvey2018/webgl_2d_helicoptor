"use strict"; 
//code for the rain
class Rain extends GameObject {
 // initialisation
 constructor() {
	super();
	
	this.position = [0,0];
	this.rotation = 0;
	this.scale = .125;
	
	
        const nSides = 16;
        this.points = new Float32Array(nSides * 2);

        for (let i = 0; i < nSides; i++) {
            this.points[2*i] = Math.cos(i*(2*Math.PI)/(nSides));     
            this.points[2*i+1] =Math.sin(i*(2*Math.PI)/nSides);   
	
	
	
 }
 }
 
 // update the rain
 update(deltaTime) {
 check(isNumber(deltaTime));
 //the rain falls from random x and y coordinates, and slowly gows smaller. it moves in the positive x and negative y direction
this.position[0] += .5;
this.position[1] -= .5;
 this.scale-=0.05;
 if (this.scale <= 0) {
	this.scale = .125
	this.position[0] = Math.floor(Math.random() * 41) -20;      // returns a random integer from 0 to 10
	this.position[1] = Math.floor(Math.random() * 16) -7.5;      // returns a random integer from 0 to 10
	
 }
 
 
 
 
 }
 // draw the rain
 render(gl, worldMatrixUniform, colourUniform) {
 check(isContext(gl),
 isUniformLocation(worldMatrixUniform, colourUniform));

 
 let matrix = Matrix.trs(
 this.position[0], this.position[1],
 0, this.scale, this.scale*2);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);
 
 gl.uniform4fv(colourUniform, [0,0,1,1]);

 
gl.bufferData(gl.ARRAY_BUFFER, this.points, gl.STATIC_DRAW);
gl.drawArrays(gl.TRIANGLE_FAN,0,this.points.length/2);


 
 
 
 }
}