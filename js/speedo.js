"use strict";
//code for the HUD display that acts as a speedometer for the helicopter
class Speedometer extends GameObject {
 // initialisation
 constructor() {
	super();
	
	this.position = [0,0];
	this.rotation = 0;
	this.flight = false;
	
	
        const nSides = 8;
        this.points = new Float32Array(nSides * 2);

        for (let i = 0; i < nSides; i++) {
            this.points[2*i] = Math.cos(i*(2*Math.PI)/(nSides));     
            this.points[2*i+1] =Math.sin(i*(2*Math.PI)/nSides);   
	
	
	
 }
 }
 
 
 // update the speedo on each frame
 update(deltaTime, heliFlight) {
 
 //helicoptor = new Helicoptor();
 
 check(isNumber(deltaTime));
 
 
 

 
//if helicoptor is flying and up botton is pressed= rotate the speedo!
if(heliFlight == true){
 if (Input.upPressed) {
 this.rotation-=0.05

 
 if(this.rotation <=-2.6)
	this.rotation = -2.6
 }
 //if up botton is released: slowly come back to 0!
 else if (!Input.upPressed) {
	this.rotation +=0.05;
	if (this.rotation>0) {
	this.rotation = 0;
	}
 }
 }
 else if (heliFlight==false) { //if heli is not moving, no speed- put him back to 0!
	this.rotation =0;
 }
 
 }
 
 
 
 
 // draw the hud
 render(gl, worldMatrixUniform, colourUniform) {
 check(isContext(gl),
 isUniformLocation(worldMatrixUniform, colourUniform));
 
 // set the uniforms
 //let matrix = Matrix.trs(
 //this.position[0], this.position[1],
 //0, 1, 1);
 //gl.uniformMatrix3fv(
 //worldMatrixUniform, false, matrix);
 
 
 
 //set uniform for hud body and colour to grey
 
 let matrix = Matrix.trs(
 this.position[0]+19, this.position[1]-5,
 0, this.scale, this.scale);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);
 
 gl.uniform4fv(colourUniform, [0.67,0.67,0.67,1]);

 
gl.bufferData(gl.ARRAY_BUFFER, this.points, gl.STATIC_DRAW);
gl.drawArrays(gl.TRIANGLE_FAN,0,this.points.length/3);

//set uniform for hud line and colour to black
 
 matrix = Matrix.trs(
 this.position[0]+19, this.position[1]-5,
 this.rotation, this.scale, this.scale);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);
 
 gl.uniform4fv(colourUniform, [0,0,0,1]);

 
 const line = new Float32Array([0,0,-0.75,0,-0.75,0.5]);
 gl.bufferData(gl.ARRAY_BUFFER,
 line, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, line.length / 2);

 
 
 
 }
}