"use strict";

/**
 * Input manager.
 * 
 * This class listens for keydown and keyup events to keep track of whether the arrow keys are currently pressed.
 * 
 * Usage:
 * 
 * if (Input.leftPressed) {
 *     // the left arrow is pressed
 * }
 */

const InputClass = function() {
    const input = this;

    input.leftPressed = false;
    input.rightPressed = false;
    input.upPressed = false;
    input.downPressed = false;
	input.cPressed = false;
	
	input.lpressed = false;
	input.Lpressed = false;
	input.tPressed = false;
	input.Tpressed = false;;

    input.onKeyDown = function(event) {
        switch (event.key) {
            case "ArrowLeft": 
                input.leftPressed = true;
                break;

            case "ArrowRight": 
                input.rightPressed = true;
                break;

            case "ArrowDown":
                input.downPressed = true;
                break;

            case "ArrowUp":
                input.upPressed = true;
                break;
			case "c":
				input.cPressed = true;
				
			case "l":
				input.lPressed = true;
				
			case "L":
				input.Lpressed = true;
				
			case "t":
				input.tPressed = true;
				
			case "T":
				input.Tpressed = true;
        }
    }

    input.onKeyUp = function(event) {
        switch (event.key) {
            case "ArrowLeft": 
                input.leftPressed = false;
                break;

            case "ArrowRight": 
                input.rightPressed = false;
                break;

            case "ArrowDown":
                input.downPressed = false;
                break;

            case "ArrowUp":
                input.upPressed = false;
                break;
				
			case "c":
				input.cPressed = false;
				
			case "l":
				input.lPressed = false;
				
			case "L":
				input.Lpressed = false;
				
			case "t":
				input.tPressed = false;
				
			case "T":
				input.Tpressed = false;
        }
    }

    document.addEventListener("keydown", input.onKeyDown);
    document.addEventListener("keyup", input.onKeyUp);

}

// global inputManager variable
const Input = new InputClass();