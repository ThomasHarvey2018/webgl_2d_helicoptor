"use strict";

class Helicoptor extends GameObject{
 // initialisation
 constructor() {
	super();
	this.position = [-18,3];
	this.rotation = 0;
	this.speed = 0;
	this.scale=0.8
	this.bladesRotation = 0;
	let upPressed = false;
	let downPressed = false;
	
	this.flight = false;
	
 }
 // update the heli on each frame
 update(deltaTime) {
	//control whether heli is flying or not
 if (this.flight == false && ((Input.tpressed==true)|| (Input.Tpressed)==true)) {
	this.flight = true;
 }
 
 if (this.flight == true && ((Input.lpressed==true)|| (Input.Lpressed)==true)) {
	this.flight = false;
 }
 
 if (this.flight==false) {
	this.scale -=0.05;
	if (this.scale<0.8) {
		this.scale=0.8
	}
 }
 
 
 if (this.flight == true) {
 
 this.scale+=0.05
 if (this.scale>1.0)
	this.scale=1;

 check(isNumber(deltaTime));
 this.bladesRotation += 0.05;
 //console.log(this.rotation);
 //blades.update(deltaTime);
 
 // copy the old position
 let oldX = this.position[0];
 let oldY = this.position[1];
  
 if (Input.leftPressed) {
 this.rotation += 0.05;
 }
 if (Input.rightPressed) {
 this.rotation -= 0.05;
 }
 if (Input.upPressed) {
	this.upPressed = true;
 
 
 this.speed +=.5;
 
 if (this.speed >=2) {
	this.speed ==2;
 }
 
 this.position[0] +=
 Math.cos(this.rotation) * this.speed * deltaTime;
 this.position[1] +=
 Math.sin(this.rotation) * this.speed * deltaTime;
 }
 
else if (this.upPressed == true) {
 
	this.speed -=.5;
 
 if (this.speed <=0) {
	this.speed =0;
	
	this.upPressed=false;
 }
 
 this.position[0] +=
 Math.cos(this.rotation) * this.speed * deltaTime;
 this.position[1] +=
 Math.sin(this.rotation) * this.speed * deltaTime;
	
}

}
	
 }
 
 // draw the helicoptor
 render(gl, worldMatrixUniform, colourUniform) {
 check(isContext(gl),
 isUniformLocation(worldMatrixUniform, colourUniform));
 
 // set the uniforms
 let matrix = Matrix.trs(
 this.position[0], this.position[1],
 this.rotation, this.scale, this.scale);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);
 
 
 
 //set colour to brown
 gl.uniform4fv(colourUniform, [0.46,0.12,0.12,1]);

 // draw the body
 const body = new Float32Array([0,1,0,-1,1,0, 0,0,-1,1,0,1,0,0,-1,-1,0,-1,
								0,1,-1.4,0,0,-1	]);
 gl.bufferData(gl.ARRAY_BUFFER,
 body, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, body.length / 2);
 
 //set colour to grey
 gl.uniform4fv(colourUniform, [0.67,0.67,0.67,1]);
 
 //draw the rotors
 
  // set the topblade uniforms
 matrix = Matrix.trs(
 this.position[0], this.position[1],
 this.bladesRotation, this.scale, this.scale);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);

 const toprotors = new Float32Array([-0.1,-0.6,-0.1,0.6,0.1,0.6,0.1,0.6,-0.1,-0.6,0.1,-0.6]);
 gl.bufferData(gl.ARRAY_BUFFER,
 toprotors, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, toprotors.length / 2);
 
 // set the botblade uniforms
 matrix = Matrix.trs(
 ((Math.cos(this.rotation+3*Math.PI/4) - Math.sin(this.rotation+3*Math.PI/4))+this.position[0]), ((Math.sin(this.rotation+3*Math.PI/4) + Math.cos(this.rotation+3*Math.PI/4))+this.position[1]),
 this.bladesRotation*(-1), this.scale, this.scale)
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);

 
 const botrotors = new Float32Array([-0.1,0.3, -0.1,-0.3,0.1,0.3, 0.1, 0.3, -0.1,-0.3,0.1,-0.3]);
 gl.bufferData(gl.ARRAY_BUFFER,
 botrotors, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, botrotors.length / 2);
 
 }
}