"use strict";

class Terrain {
 // initialisation
 constructor() {
	
	this.position = [0,0];
	this.rotation = 0;	
 }
 
 // draw the terrain
 render(gl, worldMatrixUniform, colourUniform) {
 check(isContext(gl),
 isUniformLocation(worldMatrixUniform, colourUniform));
 
 // set the terrain uniforms
 let matrix = Matrix.trs(
 this.position[0], this.position[1],
 this.rotation, 1, 1);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);
 
 //set colour to black
 gl.uniform4fv(colourUniform, [0,0,0,1]);
 
 //draw helipad

 const helipad = new Float32Array([-20.5,5.5,-20.5,0.5,-15.5,5.5,-15.5,5.5,-20.5,0.5,-15.5,0.5]);
 gl.bufferData(gl.ARRAY_BUFFER,
 helipad, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, helipad.length / 2);
 
  gl.uniform4fv(colourUniform, [1,1,1,1]);
 
 const helipadH = new Float32Array([-19.5,1.5,-19.5,4.5,-18.5,1.5,-18.5,1.5,-19.5,4.5,-18.5,4.5,
									-18.5,3.5,-17.5,3.5,-17.5,2.5,-17.5,2.5,-18.5,2.5,-18.5,3.5,
									-17.5,4.5,-17.5,1.5,-16.5,4.5,-16.5,4.5,-16.5,1.5,-17.5,1.5	]);
 gl.bufferData(gl.ARRAY_BUFFER,
 helipadH, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, helipadH.length / 2);
 
 //set colour to orange
 gl.uniform4fv(colourUniform, [0.82,0.44,0.15,1]);
 
 //draw houses

 const h1 = new Float32Array([-13,3,-13,-2,-8,3,-8,3,-13,-2,-8,-2]);
 gl.bufferData(gl.ARRAY_BUFFER,
 h1, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, h1.length / 2);
 
 gl.uniform4fv(colourUniform, [0,0,0,1]);
 
 const h1x = new Float32Array([-12,2,-9,-1, -12,-1, -9,2]);
 gl.bufferData(gl.ARRAY_BUFFER,
 h1x, gl.STATIC_DRAW);
 gl.drawArrays(gl.LINES, 0, h1x.length / 2);

 
 //set colour to orange
 gl.uniform4fv(colourUniform, [0.82,0.44,0.15,1]);
 
 const h2 = new Float32Array([5,-2,5,-7,10,-2,10,-2,5,-7,10,-7]);
 gl.bufferData(gl.ARRAY_BUFFER,
 h2, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, h2.length / 2);
 
 gl.uniform4fv(colourUniform, [0,0,0,1]);
 
 const h2x = new Float32Array([6,-3,9,-6, 6,-6, 9, -3]);
 gl.bufferData(gl.ARRAY_BUFFER,
 h2x, gl.STATIC_DRAW);
 gl.drawArrays(gl.LINES, 0, h2x.length / 2);
 
 //set colour to orange
 gl.uniform4fv(colourUniform, [0.82,0.44,0.15,1]);
 
 const h3 = new Float32Array([-5.5,0,-5.5,5,-0.5,0,-0.5,0,-0.5,5,-5.5,5]);
 gl.bufferData(gl.ARRAY_BUFFER,
 h3, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, h3.length / 2);
 
 gl.uniform4fv(colourUniform, [0,0,0,1]);
 
 const h3x = new Float32Array([-4.4,1,-1.5,4,-4.5,4,-1.5,1]);
 gl.bufferData(gl.ARRAY_BUFFER,
 h3x, gl.STATIC_DRAW);
 gl.drawArrays(gl.LINES, 0, h3x.length / 2);
 
 //draw river
 gl.uniform4fv(colourUniform, [0,0,1,1]);
 const riv = new Float32Array([-20,-20,20,20,25,20,25,20,-20,-20,-15,-20]);
 gl.bufferData(gl.ARRAY_BUFFER,
 riv, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, riv.length / 2);
 
 /* // potential code for riverbank but its flooded!
 gl.uniform4fv(colourUniform, [.94,0.69,0.13,1]);
 const riv1 = new Float32Array([-21,-20,-20,-20,20,20,20,20,-21,20,19,20]);
 gl.bufferData(gl.ARRAY_BUFFER,
 riv1, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, riv1.length / 2);
 
 const riv2 = new Float32Array([-15,-20,-14.5,-20,25.5,20,25.5,20,25,20,-15,-20]);
 gl.bufferData(gl.ARRAY_BUFFER,
 riv2, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0, riv2.length / 2);
 */

 
 }
}
