"use strict";
//Assignment One: Thomas Harvey, 41495020
// Shader code

const vertexShaderSource = `
attribute vec4 a_position;
uniform mat3 u_worldMatrix;
uniform mat3 u_viewMatrix;

void main() {
    // convert to homogeneous coordinates
 vec3 p = vec3(a_position.xy, 1);
 // multiply by world martix
 p = u_worldMatrix * p;
 //mulitply by view matrix
 p = u_viewMatrix * p;
// output to gl_Position
 gl_Position = vec4(p.xy, 0, 1);
}
`;

const fragmentShaderSource = `
precision mediump float;
uniform vec4 u_colour;

void main() {
  gl_FragColor = u_colour; 
}
`;

function createShader(gl, type, source) {
    check(isContext(gl), isString(source));

    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
        console.error(gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }
    return shader;
}

function createProgram(gl, vertexShader, fragmentShader) {
    check(isContext(gl), isShader(vertexShader, fragmentShader));

    const program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    const success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        console.error(gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
        return null;
    }
    return program;
}

function main() {

    // === Initialisation ===
	const resolution = 25// pixels per unit

    // get the canvas element & gl rendering 
    const canvas = document.getElementById("c");
    const gl = canvas.getContext("webgl");
	
    if (gl === null) {
        window.alert("WebGL not supported!");
        return;
    }
	
	//create game objects
	const raindrop = new Rain();
	const raindrop1 = new Rain();
	const raindrop2 = new Rain();
	const raindrop3 = new Rain();
	const raindrop4 = new Rain();
	const heli = new Helicoptor();
	const terr = new Terrain();
	const mini = new Minimap();
	//attach a camera to the heli -- FEATURE NOT IMPLEMENTED IN THE END.. :( 
	const camera = new GameObject();
	const pivot = new GameObject();
	camera.parent = pivot;
	let cameraHeli=false;
	const hud = new Speedometer();
	hud.scale=2;
	
    
    // create GLSL shaders, upload the GLSL source, compile the shaders
    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);
    const program =  createProgram(gl, vertexShader, fragmentShader);
    gl.useProgram(program);


    // Initialise the shader attributes & uniforms
    const positionAttribute = gl.getAttribLocation(program, "a_position");
    let worldMatrixUniform =
		gl.getUniformLocation(program, "u_worldMatrix");
	const colourUniform = gl.getUniformLocation(program,"u_colour");
	const viewMatrixUniform = gl.getUniformLocation(program,"u_viewMatrix");

    // Initialise the array buffer
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.enableVertexAttribArray(positionAttribute);
    gl.vertexAttribPointer(positionAttribute, 2, gl.FLOAT, false, 0, 0);
	
	
	
    
    // === Per Frame operations ===

    // update objects in the scene
    let update = function(deltaTime) {
        check(isNumber(deltaTime));

        // update the gameObjecs
		heli.update(deltaTime);
		hud.update(deltaTime, heli.flight);
		raindrop.update(deltaTime);
		raindrop1.update(deltaTime);
		raindrop2.update(deltaTime);
		raindrop3.update(deltaTime);
		raindrop4.update(deltaTime);
	//unimplemented feature..
		if (Input.cPressed && cameraHeli == false) {
				
				
				cameraHeli = true;
				}
		else if (Input.cPressed && cameraHeli == true) {
				
				
				cameraHeli = false;
				}
		
    };

    // redraw the scene
    let render = function() {
	resizeCanvas(canvas);
        // clear the screen
        gl.viewport(0, 0, canvas.width, canvas.height);        
        gl.clearColor(0.105, 0.56, 0.03, 1);
        gl.clear(gl.COLOR_BUFFER_BIT);
		
// scale the view matrix to the canvas size

 const sx = 2 * resolution / canvas.width;
 const sy = 2 * resolution / canvas.height;
 
 
 
 
 
 
 
 
 

 camera.parent = pivot;
 let viewMatrix = Matrix.scale(sx, sy);
 for (let o = camera; o != null; o = o.parent) {
 viewMatrix = Matrix.multiply(viewMatrix,
 Matrix.rotation(-o.rotation));
 viewMatrix = Matrix.multiply(viewMatrix,
 Matrix.translation(
 -o.translation[0], -o.translation[1]));
 }
 
  gl.uniformMatrix3fv(
 viewMatrixUniform, false, viewMatrix);
 
 terr.render(gl,worldMatrixUniform, colourUniform);
 heli.render(gl, worldMatrixUniform, colourUniform);
 raindrop.render(gl,worldMatrixUniform, colourUniform);
 raindrop1.render(gl,worldMatrixUniform, colourUniform);
 raindrop2.render(gl,worldMatrixUniform, colourUniform);
 raindrop3.render(gl,worldMatrixUniform, colourUniform);
 raindrop4.render(gl,worldMatrixUniform, colourUniform);
 
  hud.render(gl,worldMatrixUniform, colourUniform);
 
 
 mini.render(gl, canvas.width/4, canvas.height/4, terr, heli, worldMatrixUniform, colourUniform);
	//re-render the raindrops so they are shown in the minimap..
 raindrop.render(gl,worldMatrixUniform, colourUniform);
 raindrop1.render(gl,worldMatrixUniform, colourUniform);
 raindrop2.render(gl,worldMatrixUniform, colourUniform);
 raindrop3.render(gl,worldMatrixUniform, colourUniform);
 raindrop4.render(gl,worldMatrixUniform, colourUniform);


		
		

        // set the uniforms
        
		let matrix = Matrix.identity();
		gl.uniformMatrix3fv(
			worldMatrixUniform, false, matrix);

       


    };

    // animation loop
    let oldTime = 0;
    let animate = function(time) {
        check(isNumber(time));
        
        time = time / 1000;
        let deltaTime = time - oldTime;
        oldTime = time;

        resizeCanvas(canvas)
        update(deltaTime);
        render();

        requestAnimationFrame(animate);
    }

    // start it going
    animate(0);
}    

//taken from lecture! keep that ratio looking good..
function resizeCanvas(canvas) {
	check (isCanvas(canvas));

	const cssToRealPixels = window.devicePixelRatio || 1.0;
	const displayWidth = Math.floor(canvas.clientWidth * cssToRealPixels);
	const displayHeight = Math.floor(canvas.clientHeight * cssToRealPixels);
	
	if(canvas.width !== displayWidth || canvas.height !== displayHeight) {
	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;
	return true;
	}
	else {
		return false;
	}
}

