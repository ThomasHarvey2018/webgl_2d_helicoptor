class Minimap { //code for the minimap, located in the bottom left of the screen
	render(gl, width, height, terr, heli, worldMatrixUniform, colourUniform) {
		
		
	gl.viewport(1, 1, width-10, height-10);        
        	
	terr.render(gl,worldMatrixUniform, colourUniform);
	heli.render(gl, worldMatrixUniform, colourUniform);
	
let matrix = Matrix.trs(
 0, 0,
 0, 1, 1);
 gl.uniformMatrix3fv(
 worldMatrixUniform, false, matrix);
 
 gl.uniform4fv(colourUniform, [0,0,0,1]);

 // draw the borders for the minimap
 const maptri = new Float32Array([-26,-25,-26,12,-25,12,-25,12,-25,-25,-26,-25,
								  //-57,8.5,25,7.5,25,8.5,-57,7.5,-57,8.5,25,7.5,
									26,12,25,12,26,-25,26,12,25,12,25,-25									
																			]);
 gl.bufferData(gl.ARRAY_BUFFER,
 maptri, gl.STATIC_DRAW);
 gl.drawArrays(gl.TRIANGLES, 0,maptri.length / 2);
	
	}
}